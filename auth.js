// const jwt= require("jsonwebtoken");

// const secret = "CourseBookingAPI";

// //JSON web TOkens
// //token Creation

// module.exports.createAccessToken = (user) =>{

// 	const data = {
// 		id: user._id,
// 		email: user.email,
// 		isAdmin: user.isAdmin
// 	}
// /// generates a token= jwt.sign(<payload> <secret> user defined <{}can be blank or set time formate is {expiresInL "number plus m for minute h for hour"} )
// 	return jwt.sign(data, secret, {})
// }

// // token verification
// module.exports.verify =(req,res,next) => {

// 	let token =req.headers.authorization

// 	if (typeof token !== "underfined") {
// 		console.log(token)
// 		token= token.slice(7, token.length);

// 		return jwt.verify(token, secret, (err,data) =>{
// 			if (err){
// 				return res.send({auth:"failed"})
// 			}
// 			else{
// 				next()
// 			}
// 		})
// 	}
// 	else{
// 		return res.send({auth:"failed"})
// 	}
// }


// // token desciption
// module.exports.decode = (token) =>{
// 	if (typeof token !== "underfined"){
// 		token=token.slice(7,token.length);
// 		return jwt.verify(token, secret,(err,data) =>{
// 			if (err){
// 				return null
// 			}
// 			else{

// 				return jwt.decode(token, {complete:true}).payload;
// 			}
// 		})
// 	}
// 	// else {
// 		return null
// 	}
// }


const jwt = require("jsonwebtoken");
// User defined string data that will be used to create our JSON web tokens
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
const secret = "CourseBookingAPI";


//JSON Web Tokens

//Token Creation
module.exports.createAccessToken = (user) => {
	console.log(user)
	/*
		{
		  _id: new ObjectId("634015b461e9860026ee3c44"),
		  firstName: 'Jane',
		  lastName: 'Hufano',
		  email: 'janehufano@mail.com',
		  password: '$2b$10$jgneYUAwtGwpbfY4VwqNg.5lFOekR.v1AS3kJlxNhX54.eVlp/Bd2',
		  isAdmin: false,
		  mobileNo: '09123456789',
		  enrollments: [],
		  __v: 0
		}
	*/
	// The data will be received from the registration form
	// When the user logs in, a token will be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//Generate a token
	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {})



}



//Token Verification
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization


	if (typeof token !== "undefined" ) {

		console.log(token)

		token = token.slice(7, token.length);


		return jwt.verify(token, secret, (err, data) => {

			if(err) {
				return res.send({auth: "failed"})
			} else {

				next()
			}

		})


	} else {

		return res.send({auth: "failed"});
	}

}




//Token Decryption
module.exports.decode = (token) => {

	if (typeof token !== "undefined") {

		token = token.slice(7, token.length);


		return jwt.verify(token, secret, (err, data) => {

				if(err) {
					return null

				} else {

					return jwt.decode(token, {complete: true}).payload;
				}

		})


	} else {

		return null
	}

}