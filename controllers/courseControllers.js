const Course = require("../model/Course");


// Create a New Course

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});


	return newCourse.save().then( (course, error) => {

		if(error) {

			return false

		} else {

			return true
		}

	});
}


//retrieve all acourses
module.exports.getAllCourses= () =>{
	return Course.find({}).then(result =>{
		return result;
	})
};




//control for retrieving active courses

module.exports.getAllActive = ()=>{
	return Course.find({isActive: true}).then(result=>{
		return result
	})
} 


// retrieving a specific course
module.exports.getCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}

//updating course
module.exports.updateCourse = (reqParams,reqBody,data) =>{
	if(data.isAdmin===true){
		let updatedCourse = {
			name: reqBody.name,
			description:reqBody.description,
			price: reqBody.price
		};
	
	return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,error) =>{
		if (error){
			return false
		}else{
			return true
		}
	})
	}
	else {

		return false
	}

}